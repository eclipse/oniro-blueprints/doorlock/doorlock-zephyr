/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Grzegorz Gwozdz <grzegorz.gwozdz@huawei.com>
 */

#ifdef CONFIG_ROTATING_DOORLOCK

#include "rotating_doorlock.hpp"
#include "doorlock.hpp"
#include <cstddef>
#include <memory>

#define DOORLOCK0_NODE DT_ALIAS(doorlock0)
#if DT_NODE_HAS_STATUS(DOORLOCK0_NODE, okay)
#define DOORLOCK0 DT_GPIO_LABEL(DOORLOCK0_NODE, gpios)
#define PIN0 DT_GPIO_PIN(DOORLOCK0_NODE, gpios)
#define FLAGS0 DT_GPIO_FLAGS(DOORLOCK0_NODE, gpios)
#else
#error "Unsupported board: doorlock0 devicetree alias is not defined"
#endif

#define DOORLOCK1_NODE DT_ALIAS(doorlock1)
#if DT_NODE_HAS_STATUS(DOORLOCK1_NODE, okay)
#define DOORLOCK1 DT_GPIO_LABEL(DOORLOCK1_NODE, gpios)
#define PIN1 DT_GPIO_PIN(DOORLOCK1_NODE, gpios)
#define FLAGS1 DT_GPIO_FLAGS(DOORLOCK1_NODE, gpios)
#else
#error "Unsupported board: doorlock1 devicetree alias is not defined"
#endif

/**
 * @brief Lock Rotating implementation
 *
 */

void lock_stop_rotating_timer_handler(k_timer *timer_id)
{
    rotating_doorlock::timer_wrapper *wrapper =
        CONTAINER_OF(timer_id, rotating_doorlock::timer_wrapper, timer);
    wrapper->lock->lock_rotating_stop();
}

rotating_doorlock::rotating_doorlock() : doorlock(), wrapper(this), 
                                       Pin0(device_get_binding(DOORLOCK0)),
                                       Pin1(device_get_binding(DOORLOCK1))
{

    k_timer_init(&this->wrapper.timer, lock_stop_rotating_timer_handler, NULL);

    if (!this->Pin0) {
        printk("Can't get devicetree entry for doorlock0\n");
        throw(ENODEV);
	}

    if (!this->Pin1) {
        printk("Can't get devicetree entry for doorlock0\n");
        throw(ENODEV);
	}

    if (gpio_pin_configure(this->Pin0, PIN0,
                           GPIO_OUTPUT_ACTIVE | FLAGS0) < 0) {
        printk("Can't configure motor pin 0\n");
        throw(ENODEV);
    }

    if (gpio_pin_configure(this->Pin1, PIN1,
                           GPIO_OUTPUT_ACTIVE | FLAGS1) < 0) {
        printk("Can't configure motor pin 1\n");
        throw(ENODEV);
    }
}

void rotating_doorlock::lock_rotating_rotate_clockwise()
{
    gpio_pin_configure(this->Pin1, PIN1, FLAGS1);
    gpio_pin_configure(this->Pin0, PIN0, FLAGS0 | GPIO_OUTPUT_ACTIVE | GPIO_ACTIVE_HIGH);
}

void rotating_doorlock::lock_rotating_rotate_counterclockwise()
{
    gpio_pin_configure(this->Pin0, PIN0, FLAGS0);
    gpio_pin_configure(this->Pin1, PIN1, FLAGS1 | GPIO_OUTPUT_ACTIVE | GPIO_ACTIVE_HIGH);
}

void rotating_doorlock::lock_rotating_stop()
{
	// No point in leaving an auto-stop running if we've already stopped it
    k_timer_stop(&(wrapper.timer));
    gpio_pin_set(this->Pin0, PIN0, 0);
    gpio_pin_set(this->Pin1, PIN1, 0);
}

void rotating_doorlock::unlock()
{
    this->doorlock::unlock();

    this->lock_rotating_rotate_counterclockwise();
    // It takes roughly 13 seconds to go from fully closed to fully open
    k_timer_start(&this->wrapper.timer, K_SECONDS(13), K_NO_WAIT);
}

void rotating_doorlock::lock()
{
    this->doorlock::lock();

    this->lock_rotating_rotate_clockwise();
    // It takes roughly 13 seconds to go from fully open to fully closed
    k_timer_start(&this->wrapper.timer, K_SECONDS(13), K_NO_WAIT);
}

void rotating_doorlock::unlock_lock(int time)
{
    this->lock();
    //We have to wait for at least 13 seconds
    if (time < 13)
        time = 13;
    k_timer_start(&this->wrapper.timer, K_SECONDS(time), K_NO_WAIT);
    this->unlock();
}

#endif
