/* Copyright (C) 2022 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Luca Seritan <luca.seritan@huawei.com>
 */

#ifndef KEYPAD_INPUT_HPP
#define KEYPAD_INPUT_HPP

#ifdef CONFIG_KEYPAD_INPUT

#include "input_device.hpp"
#include <zephyr/drivers/gpio.h>
#include <zephyr/net/socket.h>

class keypad_input : public input_device {
public:  
   
    keypad_input();

    long read_event() const;

    int fd() const noexcept;
  
private:
    void process();

    int _m_fd[2];
    const device *_m_gpio_dev;

    /*
        Wrapper used to reference this object from within callback
    */
    struct handler_wrapper {
        gpio_callback cb;
        keypad_input *input;
        
        handler_wrapper() {
            
        }

        handler_wrapper(gpio_callback _cb, keypad_input *_input = 0) {
            cb = _cb;
            input = _input;
        }
    };

    handler_wrapper _m_wrapper;

    friend void gpio_handler(const device* port,
                            gpio_callback *cb,
                            gpio_port_pins_t pins);
};

#endif // CONFIG_KEYPAD_INPUT

#endif // KEYPAD_INPUT_HPP
