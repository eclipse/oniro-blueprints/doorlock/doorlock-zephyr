/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 *
 * Backports of some Zephyr 2.7+ APIs to older versions
 */
#pragma once

#include <version.h>

#if KERNELVERSION < 0x2070000
#include <drivers/gpio.h>
static inline int gpio_pin_get_dt(const struct gpio_dt_spec *spec)
{
    return gpio_pin_get(spec->port, spec->pin);
}

static inline int gpio_pin_set_dt(const struct gpio_dt_spec *spec, int value)
{
    return gpio_pin_set(spec->port, spec->pin, value);
}
#endif

